# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(984, 733)
        MainWindow.setWindowOpacity(1.0)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.Plot = QtGui.QWidget(self.centralwidget)
        self.Plot.setMinimumSize(QtCore.QSize(517, 541))
        self.Plot.setObjectName(_fromUtf8("Plot"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.Plot)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout.addWidget(self.Plot)
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setMinimumSize(QtCore.QSize(162, 0))
        self.widget.setMaximumSize(QtCore.QSize(162, 16777215))
        self.widget.setObjectName(_fromUtf8("widget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.widget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.groupBox = QtGui.QGroupBox(self.widget)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.listDevicesButton = QtGui.QPushButton(self.groupBox)
        self.listDevicesButton.setObjectName(_fromUtf8("listDevicesButton"))
        self.verticalLayout_4.addWidget(self.listDevicesButton)
        self.connectToSelectedButton = QtGui.QPushButton(self.groupBox)
        self.connectToSelectedButton.setObjectName(_fromUtf8("connectToSelectedButton"))
        self.verticalLayout_4.addWidget(self.connectToSelectedButton)
        self.deviceListView = QtGui.QListView(self.groupBox)
        self.deviceListView.setMinimumSize(QtCore.QSize(0, 80))
        self.deviceListView.setObjectName(_fromUtf8("deviceListView"))
        self.verticalLayout_4.addWidget(self.deviceListView)
        self.verticalLayout.addWidget(self.groupBox)
        self.measureButton = QtGui.QPushButton(self.widget)
        self.measureButton.setMinimumSize(QtCore.QSize(0, 30))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.measureButton.setFont(font)
        self.measureButton.setObjectName(_fromUtf8("measureButton"))
        self.verticalLayout.addWidget(self.measureButton)
        self.ConfigButton = QtGui.QPushButton(self.widget)
        self.ConfigButton.setMinimumSize(QtCore.QSize(0, 30))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.ConfigButton.setFont(font)
        self.ConfigButton.setObjectName(_fromUtf8("ConfigButton"))
        self.verticalLayout.addWidget(self.ConfigButton)
        self.verticalGroupBox = QtGui.QGroupBox(self.widget)
        self.verticalGroupBox.setFlat(False)
        self.verticalGroupBox.setCheckable(False)
        self.verticalGroupBox.setObjectName(_fromUtf8("verticalGroupBox"))
        self.verticalLayout_5 = QtGui.QVBoxLayout(self.verticalGroupBox)
        self.verticalLayout_5.setMargin(3)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.formLayout_2 = QtGui.QFormLayout()
        self.formLayout_2.setObjectName(_fromUtf8("formLayout_2"))
        self.label = QtGui.QLabel(self.verticalGroupBox)
        self.label.setObjectName(_fromUtf8("label"))
        self.formLayout_2.setWidget(0, QtGui.QFormLayout.LabelRole, self.label)
        self.SampleNoLineEdit = QtGui.QLineEdit(self.verticalGroupBox)
        self.SampleNoLineEdit.setObjectName(_fromUtf8("SampleNoLineEdit"))
        self.formLayout_2.setWidget(0, QtGui.QFormLayout.FieldRole, self.SampleNoLineEdit)
        self.verticalLayout_5.addLayout(self.formLayout_2)
        self.ExportTextButton = QtGui.QPushButton(self.verticalGroupBox)
        self.ExportTextButton.setMinimumSize(QtCore.QSize(0, 30))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.ExportTextButton.setFont(font)
        self.ExportTextButton.setObjectName(_fromUtf8("ExportTextButton"))
        self.verticalLayout_5.addWidget(self.ExportTextButton)
        self.ExportXlsButton = QtGui.QPushButton(self.verticalGroupBox)
        self.ExportXlsButton.setMinimumSize(QtCore.QSize(0, 30))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.ExportXlsButton.setFont(font)
        self.ExportXlsButton.setObjectName(_fromUtf8("ExportXlsButton"))
        self.verticalLayout_5.addWidget(self.ExportXlsButton)
        self.verticalLayout.addWidget(self.verticalGroupBox)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout.addWidget(self.widget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 984, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "GPIB Instrument", None))
        self.groupBox.setTitle(_translate("MainWindow", "Connections", None))
        self.listDevicesButton.setText(_translate("MainWindow", "List devices", None))
        self.connectToSelectedButton.setText(_translate("MainWindow", "Connect to selected", None))
        self.measureButton.setText(_translate("MainWindow", "Start", None))
        self.ConfigButton.setText(_translate("MainWindow", "Config", None))
        self.verticalGroupBox.setTitle(_translate("MainWindow", "Export data", None))
        self.label.setText(_translate("MainWindow", "Sample #", None))
        self.ExportTextButton.setText(_translate("MainWindow", "Export as text", None))
        self.ExportXlsButton.setText(_translate("MainWindow", "Export as *.xls", None))

