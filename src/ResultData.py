﻿import threading
import time
import xlsxwriter
from HTMLParser import HTMLParser

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)
def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def GetTimeStamp():
	return time.strftime("%m.%d.%Y-%H.%M")

class ResultData:
	def __init__(self):
		self.stamp = 1
		self.curves = []
		self.settingsStamp = None
		self.title = ""
		self.sweepLabel = ""
		self.newData = False

	def SetTitle(self, name):
		self.title = name

	def SetSweepLabel(self, name):
		self.sweepLabel = name
	
	def Clear(self):
		self.title = ""
		self.sweepLabel = ""
		del self.curves[:]

	def MarkDataAsNew(self):
		self.newData = True

	def HaveNewData(self):
		return self.newData

	def MarkDataAsOld(self):
		self.newData = False

	def ExportAsText(self, filename, sampleName):
		text = str(sampleName) + "\t" + GetTimeStamp() + "\n"
		text += self.settingsStamp.GetConfigAsText()
		text += "====================================================================================\n"
		for curve in self.curves:
			text += "Trace: \"" + curve['name'] + "\"\n"
			text += "Unit: \"" + curve['unit'] + "\"\n"
			x = curve['x']
			y = curve['y']
			count = len(x)
			for i in range(0, count):
				text += "{:20.10f}".format(x[i]) + "\t " + "{:20.10f}".format(y[i]) + "\n"
			text += "------------------------------------------------------------------------------------\n"
		text += "====================================================================================\n"
		text_file = open(str(filename), "w")
		text_file.write(text)
		text_file.close()

	def ExportAsXls(self, filename, sampleName):
		workbook = xlsxwriter.Workbook(str(filename))
		worksheet = workbook.add_worksheet()
		titleFormat = workbook.add_format({
			'bold': 1,
			'border': 1,
			'fg_color': 'yellow'})
		dataCelleFormat = workbook.add_format({
			'border': 1,
			'fg_color': 'silver'})
		worksheet.set_column('A:F', 25)
		worksheet.merge_range('A1:B1', "Exported data from Agilent", titleFormat)
		worksheet.write('A2', "Sample:", dataCelleFormat)
		worksheet.write('B2', str(sampleName), dataCelleFormat)
		worksheet.write('A3', "Time:", dataCelleFormat)
		worksheet.write('B3', GetTimeStamp(), dataCelleFormat)
		worksheet.merge_range('A4:B4', "Commands:", titleFormat)		
		configList = self.settingsStamp.GetConfigAsList()
		i = 4
		for row in configList:
			worksheet.write(i, 0, row[0], dataCelleFormat)
			worksheet.write(i, 1, row[1], dataCelleFormat)
			i+=1

		i+=1
		worksheet.merge_range(i, 0, i, len(self.curves) * 2 -1, "Data", titleFormat)
		i+=1
		col = 0
		chart = workbook.add_chart({'type': 'line'})
		chart.set_plotarea({
			'fill':   {'color': '#000000'}
		})
		chart.set_title({'name': strip_tags(self.title)})

		colors= ['cyan', 'magenta', 'navy', 'orange', 'blue', 'lime', 'purple', 'red', 'green', 'pink']
		for curve in self.curves:
			worksheet.merge_range(i, col, i, col+1, "Trace: " + curve['name'] + " " + curve['unit'], titleFormat)	
			x = curve['x']
			y = curve['y']
			count = len(x)
			for k in range(0, count):
				worksheet.write_number(i + 1 + k, col, x[k], dataCelleFormat)
				worksheet.write_number(i + 1 + k, col + 1, y[k], dataCelleFormat)
			chart.add_series({
				'categories': ['Sheet1', i + 1, col, i + count, col],
				'values':     ['Sheet1', i + 1, col + 1, i + count, col + 1],
				'line':       {'color': colors[col/2]},
				'name':	strip_tags(curve['name'])
			})
			col +=2
		worksheet.insert_chart('D2', chart)
		chart.set_size({'width': 720, 'height': 576})
		workbook.close()