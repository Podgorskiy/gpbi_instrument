﻿import sys
from PyQt4 import QtCore, QtGui
import pyqtgraph as pg
from MainWindow import Ui_MainWindow
import numpy as np
import GPBI
import MeasureTasks
import Logger
import Config
import SettingsWindow
import ResultData

class AgilentMainWindow(QtGui.QMainWindow):
	def __init__(self, debug = False, parent=None):
		self.config = Config.Config("config.xml")
		self.config.SetProfile('Impedance')
		
		self.instrument = GPBI.Instrument(debug)
		self.currentTask = MeasureTasks.BaseTask("Dummy", None, self.config)
		self.resultData = ResultData.ResultData()
		
		QtGui.QWidget.__init__(self, parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)

		self.plot = pg.PlotWidget()
		self.ui.verticalLayout_2.addWidget(self.plot)
		
		console = QtGui.QPlainTextEdit(self.ui.Plot)
		console.setMaximumSize(QtCore.QSize(16777215, 150))
		self.ui.verticalLayout_2.addWidget(console)

		Logger.SetWriter(Logger.LogWriter(console, self.ui.statusbar))
		
		pi = self.plot.getPlotItem()
		pi.showGrid(True, True, 0.5)
		self.ui.measureButton.clicked.connect(self.MeasureStart)
		self.ui.listDevicesButton.clicked.connect(self.OnListDevices)
		self.ui.connectToSelectedButton.clicked.connect(self.OnConnectTo)
		self.ui.ConfigButton.clicked.connect(self.OnShowConfig)
		self.ui.ExportTextButton.clicked.connect(self.OnExportText)
		self.ui.ExportXlsButton.clicked.connect(self.OnExportXls)
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.Update)
		self.timer.start(100)
		
	def MeasureStart(self):
		if self.currentTask.isAlive():
			Logger.Log("Error. The last measurement is not finished! Can't start new one.");
			return
		if not self.instrument.IsConnected():
			Logger.Log("Error. Not connected to any device.");
			return
		
		self.resultData.Clear()
				
		self.currentTask = MeasureTasks.ImpedanceTask(self.instrument, self.resultData, self.config)
		Logger.Log("Measurement started...");
		self.currentTask.start()

	def OnExportText(self):
		filename = QtGui.QFileDialog.getSaveFileName(self, "Save data to text file", "", "Text files (*.txt)")
		if len(filename) == 0:
			return
		if len(self.resultData.curves) == 0:
			Logger.Log("Error. No data to export");
			return
		Logger.Log("Saving data to: " + filename)
		sampleName = self.ui.SampleNoLineEdit.text()
		self.resultData.ExportAsText(filename, sampleName)

	def OnExportXls(self):
		filename = QtGui.QFileDialog.getSaveFileName(self, "Save data to Excel file", "", "Text files (*.xlsx)")
		if len(filename) == 0:
			return
		if len(self.resultData.curves) == 0:
			Logger.Log("Error. No data to export");
			return
		Logger.Log("Saving data to: " + filename)
		sampleName = self.ui.SampleNoLineEdit.text()
		self.resultData.ExportAsXls(filename, sampleName)
		
	def OnListDevices(self):
		list = self.GetDevices()
		stringList = QtCore.QStringList()
		for i in list:
			stringList.append(i)
		stringlistModel = QtGui.QStringListModel(stringList);
		self.ui.deviceListView.setModel(stringlistModel)

	def OnConnectTo(self):
		list = self.ui.deviceListView.selectedIndexes()
		if len(list) == 0:
			 Logger.Log("Error. No device selected. You need to select device!")
			 return
		index = list[0]
		self.ConnectToDevice(index.data().toString())
	
	def OnShowConfig(self):
		settingsWindow = SettingsWindow.SettingsWindow(config = self.config)	
		settingsWindow.exec_()		

	def GetDevices(self):	
		list = self.instrument.GetResourceList()
		string = "Found the following devices: "
		for i in list[0:-1]:
			string += i + ", "
		string += list[-1] + "."
		Logger.Log(string)
		return list
	
	def ConnectToDevice(self, name):	
		Logger.Log("Connecting to " + name)
		if self.instrument.SetCurrentDevice(name):	
			Logger.Log("Connected to " + self.instrument.Query('*IDN?'))
			self.instrument.SetTimeout(100)
		else:
			Logger.Log("Can not connect to " + name)
		
	def Update(self):
		if self.currentTask.isAlive():
			return

		if not self.resultData.HaveNewData():
			return

		self.resultData.MarkDataAsOld()
				
		Logger.Log("Updating Plot")
		plot = self.plot.getPlotItem()
		plot.clear()

		plot.setTitle(self.resultData.title) 
		plot.setLabel("bottom", self.resultData.sweepLabel) 
		
		for curve in self.resultData.curves:
			c = pg.PlotCurveItem(x = curve['x'], y = curve['y'], pen = pg.mkPen({'color': curve['color'], 'width': 1}))
			plot.addItem(c)
			plot.setLabel(curve['position'], "<font size=\"10\" color=#" + curve['color'] + ">" + curve['name'] + "</font>", curve['unit'])
					
		plot.enableAutoRange('x', True)
		plot.enableAutoRange('y', True)

		Logger.Log("Measurement done!");
		
if __name__ == "__main__":
	app = QtGui.QApplication(sys.argv)
	debug = False
	if len(sys.argv)>1:
		if sys.argv[1] == '-d':
			debug = True
	appWindow = AgilentMainWindow(debug)	
	appWindow.showMaximized()
	app.exec_()
	sys.exit()
	


