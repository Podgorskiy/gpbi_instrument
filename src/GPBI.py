﻿import visa
import random
from math import *
import time 

class Instrument:
	def __init__(self, debug = False):
		self.instrument = None
		self.debug = debug
		if not debug:
			self.rm = visa.ResourceManager()
		else:
			self.fakeDevices = ["GPIB0::01::FAKE", "GPIB0::02::FAKE", "GPIB0::03::FAKE", "GPIB0::04::FAKE"]
			
	def GetResourceList(self):
		if not self.debug:
			return self.rm.list_resources()
		else:
			return self.fakeDevices
	
	def SetCurrentDevice(self, name):
		if not self.debug:
			self.instrument = self.rm.open_resource(str(name))
			return True
		self.instrument = 0
		return True			
	
	def IsConnected(self):
		return self.instrument != None
		
	def SetTimeout(self, seconds):
		if not self.debug:
			self.instrument.timeout = 1000 * seconds
	
	def Query(self, msg):
		result = ""
		if not self.debug:
			result = self.instrument.query(msg)
		else:
			if msg == '*IDN?':
				result = 'Fake device, for tests'
		return result
		
	def Write(self, msg):
		if not self.debug:
			self.instrument.write(msg)
		else:
			pass	

	def Query_ascii_values(self, msg, separator = ','):
		if not self.debug:
			result = self.instrument.query_ascii_values(str(msg), separator = str(separator))
			return result
		else:
			result = []
			time.sleep(1)
			if msg == 'OUTPDATA?':
				for i in range(0, 200):
					result.append(random.random() + exp(i / 200.0)  * i / 10.0 )
					result.append(random.random() + exp(i / 200.0) * exp(-i/100.0) * 10.0 + 1.0)
			if msg == 'OUTPSWPRM?':
				for i in range(0, 200):
					result.append(40 + i * 100)
			return result
