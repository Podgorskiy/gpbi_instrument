﻿
class LogWriter:
	def __init__(self, console = None, sb = None):
		self.console = console
		self.sb = sb
	def Write(self, msg):
		self.sb.showMessage(msg, 3000);
		self.console.appendPlainText(msg)

instance = LogWriter()

def Log(msg):
	global instance
	instance.Write(msg)
	print msg

def SetWriter(logWriter):
	global instance
	instance = logWriter