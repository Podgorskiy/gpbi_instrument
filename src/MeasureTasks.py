﻿import sys
import threading
import numpy as np
import Logger
import ResultData
import copy

class BaseTask(threading.Thread):
	def __init__(self, name, instrument, config):
		threading.Thread.__init__(self)
		self.name = name
		self.instrument = instrument
		self.config = config
	
	def Write(self, msg):
		self.instrument.Write(msg)
		
	def Query(self, msg):
		return self.instrument.Query(msg)
		
	def GetDataArray(self, msg, separator = ','):
		return np.array(self.instrument.Query_ascii_values(msg, separator))
		
class ImpedanceTask(BaseTask):
	def __init__(self, instrument, resultData, config):
		super(ImpedanceTask, self).__init__("ImpedanceTask", instrument, config)
		self.resultData = resultData 
		
	def run(self):
		for cmd in self.config.GetMeasureCommandsList():
			cmdDescription = self.config.GetCommand(cmd['name'])
			cmdStr = cmdDescription['commandMsg'] + " " + cmd['value']
			print cmdStr
			self.Write(cmdStr)
		self.Write('*CLS')
		self.Write(self.config.GetMeasureCmd())
		self.Query('*OPC?')
		
		output = self.GetDataArray('OUTPDATA?', ',')
		sweep = self.GetDataArray('OUTPSWPRM?')
		
		size  = output.size / 2
		
		print("Read " + str(size) + " data points.")
		print("Sweep array size " + str(sweep.size))
		
		R = np.zeros(size)
		X = np.zeros(size)
		
		for i in range(0, size):
			R[i] = output[2*i]
			X[i] = output[2*i + 1]
			
		self.resultData.Clear()
		self.resultData.curves.append({'x':sweep, 'y':R, 'position': 'left', 'name': 'R', 'color':'6FC', 'unit': 'ohm'})
		self.resultData.curves.append({'x':sweep, 'y':X, 'position': 'right', 'name': 'X', 'color':'F6C', 'unit': 'ohm'})
		self.resultData.MarkDataAsNew()
		self.resultData.settingsStamp = copy.deepcopy(self.config)
		self.resultData.SetTitle("<font color=#FFF size=\"16\">Impedance</font>") 
		self.resultData.SetSweepLabel("<font color=#888 size=\"10\">frequency</font>") 