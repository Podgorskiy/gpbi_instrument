﻿import xml.etree.ElementTree as ET
import xml.dom.minidom

def prettify(elem):
    rough_string = ET.tostring(elem)
    reparsed = xml.dom.minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="\t")

class Config:
	def __init__(self, filename):
		tree = ET.parse(filename)
		root = tree.getroot()
		self.commands = []
		self.profiles = []
		self.currentProfile = {}
		for el_commands in root.findall('commands'):
			for el_cmd in el_commands.findall('command'):
				cmdDict =	{
								'name':el_cmd.get('name'),
								'type':el_cmd.get('type'),
								'commandType':el_cmd.get('commandType'),
								'commandMsg':el_cmd.get('commandMsg')						
							}
				if cmdDict['type'] == 'combo':
					combo = []
					for v in el_cmd.findall('value'):
						combo.append([v.get('name'), v.get('cmd')])
					cmdDict['values'] = combo
				self.commands.append(cmdDict)
		for el_profiles in root.findall('profiles'):
			for el_profile in el_profiles.findall('profile'):
				profilesDict =	{
									'name' : el_profile.get('name')
								}
				profilesDict['commands'] = []
				for el_measureSettings in el_profile.findall('measureSettings'):
					for el_command in el_measureSettings.findall('command'):
						command = {}
						command['name'] = el_command.get('name')
						command['showInSettings'] = el_command.get('showInSettings') == "True"
						value = ""
						if self.GetCommand(command['name'])['type'] != "none":
							value = el_command.get('value')
						command['value'] = value
						profilesDict['commands'].append(command)
				self.profiles.append(profilesDict)
				for el_measureSettings in el_profile.findall('measureCommand'):
					profilesDict['measureCommand'] = el_measureSettings.get('cmd')
		if len(self.profiles) != 0:
			self.currentProfile = self.profiles[0]
					
	def SaveToFile(self, filename):
		root = ET.Element('config')
		el_commands = ET.SubElement(root, 'commands')
		for command in self.commands:
			el_cmd = ET.SubElement(el_commands, 'command')
			el_cmd.set('name', command['name'])
			el_cmd.set('type', command['type'])
			el_cmd.set('commandType', command['commandType'])
			el_cmd.set('commandMsg', command['commandMsg'])
			if command['type'] == 'combo':
				for v in command['values']:
					el_v = ET.SubElement(el_cmd, 'value')
					el_v.set('name', v[0])
					el_v.set('cmd', v[1])
		el_profiles = ET.SubElement(root, 'profiles')
		for profile in self.profiles:
			el_profile = ET.SubElement(el_profiles, 'profile')
			el_profile.set('name', profile['name'])
			el_ms = ET.SubElement(el_profile, 'measureSettings')
			for cmd in profile['commands']:
				el_cmd = ET.SubElement(el_ms, 'command')
				el_cmd.set('name', cmd['name'])
				el_cmd.set('showInSettings', 'True' if cmd['showInSettings'] else 'False')
				if self.GetCommand(cmd['name'])['type'] != "none":
					el_cmd.set('value', cmd['value'])
			el_mcmd = ET.SubElement(el_profile, 'measureCommand')
			el_mcmd.set('cmd', profile['measureCommand'])

		text_file = open(filename, "w")
		text_file.write(prettify(root))
		text_file.close()
		
	def GetCommand(self, name):
		for cmd in self.commands:
			if cmd['name'] == name:
				return cmd
		return None

	def GetProfileList(self):
		list = []
		for profile in self.profiles:
			list.append(profile['name'])
		return list

	def SetProfile(self, name):
		for profile in self.profiles:
			if profile['name'] == name:
				self.currentProfile = profile

	def GetMeasureCmd(self):
		return self.currentProfile['measureCommand']

	def GetMeasureCommandsList(self):
		return self.currentProfile['commands']

	def GetConfigAsText(self):
		text = "Profile: \"" + self.currentProfile['name'] + "\"\n"
		for command in self.currentProfile['commands']:
			text += command['name'] + ":\t\"" + command['value'] + "\"\n"
		text += "Measure command:\t\"" + self.currentProfile['measureCommand'] + "\"\n"
		return text
		
	def GetConfigAsList(self):
		list = []
		list.append(["Profile:", self.currentProfile['name']])
		for command in self.currentProfile['commands']:
			list.append([command['name'], command['value']])
		list.append(["Measure command:", self.currentProfile['measureCommand']])
		return list

