from distutils.core import setup
import py2exe
import os

setup(
		windows=[
					{
						'script': 'Main.py'
					}
				],
		includes = ['MeasureTasks.py', 'GPBI', 'MainWindow'],
		options = {
			"py2exe": {
				"dll_excludes": ["MSVCP90.dll"],
				"dist_dir": "../bin"
			}	
		}
	)
