﻿import sys
from PyQt4 import QtCore, QtGui
from Settings import Ui_SettingsDialog

class SettingsWindow(QtGui.QDialog):
	def __init__(self, config = None, parent=None):
		QtGui.QWidget.__init__(self, parent)
		self.ui = Ui_SettingsDialog()
		self.ui.setupUi(self)
		self.config = config
		self.list = self.config.GetMeasureCommandsList()
		self.controls = []
		for option in self.list:
			if option['showInSettings'] == False:
				continue
			container = QtGui.QWidget(self.ui.scrollAreaWidgetContents)
			container.setMaximumSize(QtCore.QSize(16777215, 50))
			verticalLayout_3 = QtGui.QVBoxLayout(container)
			verticalLayout_3.setMargin(3)
			innerContanier = QtGui.QWidget(container)
			horizontalLayout = QtGui.QHBoxLayout(innerContanier)
			horizontalLayout.setMargin(3)
			label = QtGui.QLabel(innerContanier)
			label.setText(option['name'])
			horizontalLayout.addWidget(label)

			cmdDescription = self.config.GetCommand(option['name'])
			if (cmdDescription['type'] == 'int'):
				spinBox = QtGui.QSpinBox(innerContanier)
				spinBox.setMaximumSize(QtCore.QSize(200, 16777215))
				spinBox.setMaximum(10000000)
				spinBox.setMinimum(-10000000)
				spinBox.setValue(int(option['value']))
				horizontalLayout.addWidget(spinBox)
				self.controls.append({'name': option['name'], 'control':spinBox})
			if (cmdDescription['type'] == 'float'):
				spinBox = QtGui.QDoubleSpinBox(innerContanier)
				spinBox.setMaximumSize(QtCore.QSize(200, 16777215))
				spinBox.setMaximum(10000000.0)
				spinBox.setMinimum(-10000000.0)
				spinBox.setValue(float(option['value']))
				horizontalLayout.addWidget(spinBox)
				self.controls.append({'name': option['name'], 'control':spinBox})
			if (cmdDescription['type'] == 'bool'):
				comboBox = QtGui.QComboBox(innerContanier)
				comboBox.setMaximumSize(QtCore.QSize(200, 16777215))
				comboBox.addItem("OFF", False)
				comboBox.addItem("ON", True)
				comboBox.setCurrentIndex(0 if option['value'] == "OFF" else 1)
				horizontalLayout.addWidget(comboBox)
				self.controls.append({'name': option['name'], 'control':comboBox})
			if (cmdDescription['type'] == 'combo'):
				comboBox = QtGui.QComboBox(innerContanier)
				comboBox.setMaximumSize(QtCore.QSize(200, 16777215))
				currentIndex = 0
				i = 0
				for item in cmdDescription['values']:
					comboBox.addItem(item[0], item[1])
					if option['value'] == item[1]:
						currentIndex = i
					i+=1
				comboBox.setCurrentIndex(currentIndex)
				horizontalLayout.addWidget(comboBox)
				self.controls.append({'name': option['name'], 'control':comboBox})

			verticalLayout_3.addWidget(innerContanier)
			line = QtGui.QFrame(container)
			line.setFrameShape(QtGui.QFrame.HLine)
			line.setFrameShadow(QtGui.QFrame.Sunken)
			verticalLayout_3.addWidget(line)
			self.ui.verticalLayout_2.addWidget(container)
		spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
		self.ui.verticalLayout_2.addItem(spacerItem)
		self.ui.OkButton.clicked.connect(self.Save)
		self.ui.CancelButton.clicked.connect(self.close)

	def Save(self):
		for control in self.controls:
			name = control['name']
			cmdDescription = self.config.GetCommand(name)
			for l in self.list:
				if l['name'] == name:
					if (cmdDescription['type'] == 'int' or cmdDescription['type'] == 'float'):
						l['value'] = str(control['control'].value())
					if (cmdDescription['type'] == 'bool'):
						l['value'] = "OFF" if control['control'].currentIndex() == 0 else "ON"
					if (cmdDescription['type'] == 'combo'):
						index = control['control'].currentIndex()
						data = control['control'].itemData(index).toString()
						l['value'] = str(data)
		self.config.SaveToFile("config.xml")
		self.close()
			